# @silmar/ng-sockets

[![npm (scoped)](https://img.shields.io/npm/v/@silmar/ng-sockets.svg)](https://www.npmjs.com/package/@silmar/ng-sockets)
[![pipeline status](https://gitlab.com/etg-public/silmar-ng-sockets/badges/master/pipeline.svg)](https://gitlab.com/etg-public/silmar-ng-sockets/commits/master)
[![NPM](https://img.shields.io/npm/l/@silmar/ng-sockets.svg?style=flat-square)](https://www.npmjs.com/package/@silmar/ng-sockets)

Socket.io Angular wrapper

### Install
#### NPM
```
npm i @silmar/ng-sockets socket.io socket.io-client
```

#### Yarn
```
yarn add @silmar/ng-sockets socket.io socket.io-client
```

### Polyfills
You should add the following into your `polyfills.ts` file

```typescript
(window as any).global = window;
```

### Original project
https://github.com/bougarfaoui/ng-socket-io
