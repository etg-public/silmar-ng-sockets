import { ModuleWithProviders, NgModule } from '@angular/core';
import { Socket } from './socket-io.service';
import { SOCKET_CONFIG_TOKEN, SocketIoConfig } from './socket-io.config';

export function SocketFactory(config: SocketIoConfig) {
  return new Socket(config);
}

@NgModule({})
export class SocketIoModule {
  static forRoot(config: SocketIoConfig): ModuleWithProviders<SocketIoModule> {
    return {
      ngModule  : SocketIoModule,
      providers : [
        { provide : SOCKET_CONFIG_TOKEN, useValue : config },
        {
          provide    : Socket,
          useFactory : SocketFactory,
          deps       : [ SOCKET_CONFIG_TOKEN ]
        }
      ]
    };
  }
}
