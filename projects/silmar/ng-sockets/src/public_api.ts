/*
 * Public API Surface of ng-sockets
 */

export * from './lib/socket-io.module';
export * from './lib/socket-io.config';
export * from './lib/socket-io.service';
