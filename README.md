# Silmar NgSockets

[![npm (scoped)](https://img.shields.io/npm/v/@silmar/ng-sockets.svg)](https://www.npmjs.com/package/@silmar/ng-sockets)
[![pipeline status](https://gitlab.com/etg-public/silmar-ng-sockets/badges/master/pipeline.svg)](https://gitlab.com/etg-public/silmar-ng-sockets/commits/master)
[![NPM](https://img.shields.io/npm/l/@silmar/ng-sockets.svg?style=flat-square)](https://www.npmjs.com/package/@silmar/ng-sockets)

### Install
```
npm i @silmar/ng-sockets
// or
yarn add @silmar/ng-sockets
```

More info in the library [README.md](projects/silmar/ng-sockets/README.md)
